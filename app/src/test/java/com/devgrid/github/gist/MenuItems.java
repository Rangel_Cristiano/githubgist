package com.devgrid.github.gist;

import static org.junit.Assert.assertEquals;

public class MenuItems {
    //Verify if is changing correctly the login for logout and vice versa
    public void changeLoginLogout(int newMenuPosition) {
        newMenuPosition = 2;
        int menuPosition = 1;

        //simulate the user is logged
        String itemNameTxt = "login";

        switch (menuPosition) {
            case 1: //login
                itemNameTxt = "logout"; //the textview of menu
                break;
            case 2: //logout
                itemNameTxt = "login"; //the textview of menu
                break;
        }
        menuPosition = newMenuPosition;

        assertEquals(menuPosition, 2);
        assertEquals(itemNameTxt, "logout");
    }
}
