package com.devgrid.github.gist;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class LoadMoreViews {
    //verify if is loading the views on the list of 10 by 10 items
    //with pagination
    @Test
    public void loadMoreViews() {
        int LOAD_VIEW_SET_COUNT = 10;
        List<Integer> list = new ArrayList<>(); //list of views

        //load the first round of gists (10 gists)
        for (int i = 0; i < LOAD_VIEW_SET_COUNT; i++) {
            list.add(i + 1);
        }

        int count = list.size();
        //trigger after the scroll and the view will load more 10 gists
        for (int i = count - 1; i < (count - 1 + LOAD_VIEW_SET_COUNT); i++) {
            list.add(i + 2);
        }

        //verify if the views are not repeated
        //start 1 because the views
        for (int j = 1; j <= 20; j++) {
            assertEquals(list.get(j - 1), new Integer(j));
        }
    }
}
