package com.devgrid.github.gist.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import com.devgrid.github.gist.R;
import com.devgrid.github.gist.adapters.DrawerHeaderAdapterView;
import com.devgrid.github.gist.adapters.DrawerMenuItemAdapterView;
import com.devgrid.github.gist.fragments.LoginDialogFragment;
import com.devgrid.github.gist.utils.IntentUtils;
import com.mindorks.placeholderview.PlaceHolderView;

// Attach the drawer to the root activity only extends it

public class DrawerActivity extends AppCompatActivity implements DrawerMenuItemAdapterView.DrawerCallBack {

    private PlaceHolderView mDrawerView;
    private DrawerLayout mDrawer;
    private Toolbar mToolbar;
    private DrawerHeaderAdapterView mHeaderAdapterView;
    private DrawerMenuItemAdapterView mPublicGistsAdapterView;
    private DrawerMenuItemAdapterView mQrCodeAdapterView;
    private DrawerMenuItemAdapterView mLoginAdapterView;
    private Boolean mRegistered;
    private DrawerMenuItemAdapterView.DrawerCallBack mCallback;

    @Override
    public void setContentView(int layoutResID)
    {
        final DrawerLayout fullView = (DrawerLayout) getLayoutInflater().inflate(R.layout.drawer_layout, null);
        final FrameLayout activityContainer = (FrameLayout) fullView.findViewById(R.id.activity_content);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(fullView);

        mDrawer = (DrawerLayout)fullView.findViewById(R.id.activity_container);
        mDrawerView = (PlaceHolderView)findViewById(R.id.drawerView);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);

        setupDrawer();
    }

    private void setupDrawer() {
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mRegistered = sharedPref.getBoolean("Registered", false);

        mHeaderAdapterView = new DrawerHeaderAdapterView(getApplicationContext());
        mPublicGistsAdapterView = new DrawerMenuItemAdapterView(getApplicationContext(),
                DrawerMenuItemAdapterView.DRAWER_MENU_ITEM_PUBLIC_GISTS, this);
        mQrCodeAdapterView = new DrawerMenuItemAdapterView(this.getApplicationContext(),
                DrawerMenuItemAdapterView.DRAWER_MENU_ITEM_QRCODE, this);
        mLoginAdapterView = new DrawerMenuItemAdapterView(this.getApplicationContext(), !mRegistered ?
                DrawerMenuItemAdapterView.DRAWER_MENU_ITEM_LOGIN :
                DrawerMenuItemAdapterView.DRAWER_MENU_ITEM_LOGOUT, this);

        mCallback = this;

        mDrawerView
                .addView(mHeaderAdapterView)
                .addView(mPublicGistsAdapterView)
                .addView(mQrCodeAdapterView)
                .addView(mLoginAdapterView);

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar,
                R.string.open_drawer, R.string.close_drawer){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                boolean registered = sharedPref.getBoolean("Registered", false);

                if (mRegistered != registered) {
                    mRegistered = registered;

                    mHeaderAdapterView.updateViews();
                    if (mRegistered) {
                        mLoginAdapterView.changeLoginLogout(DrawerMenuItemAdapterView.DRAWER_MENU_ITEM_LOGOUT);
                    } else {
                        mLoginAdapterView.changeLoginLogout(DrawerMenuItemAdapterView.DRAWER_MENU_ITEM_LOGIN);
                    }
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        mDrawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    @Override
    public void onPublicGistsMenuSelected() {
        Intent intent = new Intent(DrawerActivity.this, PublicGistsListActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLoginMenuSelected() {
        new LoginDialogFragment()
                .show(getSupportFragmentManager(), IntentUtils.EXTRA_LOGIN_DIALOG);
    }

    @Override
    public void onLogoutMenuSelected() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        finish();
        startActivity(getIntent());
    }

    @Override
    public void onQrCodeAuthentication() {
        Intent intent = new Intent(DrawerActivity.this, QrCodeScannerActivity.class);
        startActivity(intent);
    }
}
