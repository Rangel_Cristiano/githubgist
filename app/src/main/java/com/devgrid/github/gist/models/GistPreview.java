package com.devgrid.github.gist.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GistPreview implements Serializable {

    @SerializedName("id")
    @Expose
    private String mId;

    @SerializedName("description")
    @Expose
    private String mDescription;

    @SerializedName("owner")
    @Expose
    private User mUser;

    @SerializedName("created_at")
    @Expose
    private String mCreatedAt;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }
}