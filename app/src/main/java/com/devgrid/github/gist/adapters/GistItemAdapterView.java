package com.devgrid.github.gist.adapters;

import android.content.Context;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.devgrid.github.gist.R;
import com.devgrid.github.gist.models.GistPreview;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@Layout(R.layout.load_more_item_view)
public class GistItemAdapterView {

    private ItemViewCallback mCallBack;

    @View(R.id.name_text_view)
    private TextView mUserNameTextView;

    @View(R.id.detail_text_view)
    private TextView mDetailTextView;

    @View(R.id.date_text_view)
    private TextView mDateTextView;

    @View(R.id.send_comment_button)
    private Button mCommentButton;

    @View(R.id.comments_linear_layout)
    private LinearLayout mCommentsLinearLayout;

    @View(R.id.file_content)
    private TextView mFile_content;

    @View(R.id.avatar_image_view)
    private ImageView mAvatarImageView;

    private GistPreview mGistPreview;
    private Context mContext;

    public GistItemAdapterView(Context context, GistPreview gistPreview) {
        mContext = context;
        mGistPreview = gistPreview;
    }

    @Resolve
    private void onResolved() {
        mUserNameTextView.setText(mGistPreview.getUser().getLogin());
        mDetailTextView.setText(mGistPreview.getDescription());
        mDateTextView.setText(mGistPreview.getCreatedAt());
        Glide.with(mContext).load(mGistPreview.getUser().getAvatarUrl()).into(mAvatarImageView);
    }

    @Click(R.id.item_view)
    private void onItemViewClick(){
        mCallBack.onGistSelected(mGistPreview);
    }

    public void setLoadMoreCallback(ItemViewCallback callBack) {
        Log.d("DEBUG", "callback");
        mCallBack = callBack;
    }

    public interface ItemViewCallback{
        void onGistSelected(GistPreview gistPreview);
    }
}