package com.devgrid.github.gist.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.devgrid.github.gist.R;
import com.devgrid.github.gist.fragments.PublicGistsListFragment;
import com.devgrid.github.gist.models.GistPreview;
import com.devgrid.github.gist.utils.IntentUtils;

public class PublicGistsListActivity extends DrawerActivity {

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.public_gists_activity);

        // Receive the git id to call the other acitivty with the git selected
        PublicGistsListFragment.OnGistListener onGistListener = new PublicGistsListFragment.OnGistListener() {
            @Override public void onSelectGist(GistPreview gistPreview) {
                Intent intent = new Intent(PublicGistsListActivity.this, GistActivity.class);
                intent.putExtra(IntentUtils.EXTRA_GIT_ID, gistPreview.getId());
                startActivity(intent);
            }
        };

        final PublicGistsListFragment publicGistsListFragment = new PublicGistsListFragment();

        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_content, publicGistsListFragment);
        fragmentTransaction.commit();

        publicGistsListFragment.setGistsListListener(onGistListener);
    }
}
