package com.devgrid.github.gist.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.devgrid.github.gist.R;
import com.devgrid.github.gist.services.GistApiService;
import com.devgrid.github.gist.services.ServiceGenerator;
import com.devgrid.github.gist.models.Comment;
import com.devgrid.github.gist.models.Gist;
import com.devgrid.github.gist.utils.IntentUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GistFragment extends Fragment {

    //region Fields
    private static String COMMENT_EDIT_TEXT = "commentEditText";
    private String mGitId;
    private TextView mUserNameTextView;
    private TextView mDetailTextView;
    private Button mSignCommentButton;
    private EditText mCommentEditText;
    private ImageView mAvatarImageView;
    private TextView mFileView;
    private Button mSendButton;
    private LinearLayout mCommentsLinearLayout;
    //endregion

    //region Lifecycle
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mGitId = getArguments().getString(IntentUtils.EXTRA_GIT_ID);

        return inflater.inflate(R.layout.gist_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
        {
            if (mCommentEditText !=null) mCommentEditText.setText(savedInstanceState.getString(COMMENT_EDIT_TEXT));
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mUserNameTextView = view.findViewById(R.id.name_text_view);
        mDetailTextView = view.findViewById(R.id.detail_text_view);
        mAvatarImageView = view.findViewById(R.id.avatar_image_view);
        mFileView = view.findViewById(R.id.file_content);
        mCommentsLinearLayout = view.findViewById(R.id.comments_linear_layout);
        mSignCommentButton = view.findViewById(R.id.sign_comment_text_view);
        mCommentEditText = view.findViewById(R.id.comment_edit_text);
        mSendButton = view.findViewById(R.id.send_comment_button);

        //only show edit text comment if user is registered
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        final Boolean registered = sharedPref.getBoolean("Registered", false);
        if (!registered) {
            mSignCommentButton.setVisibility(View.VISIBLE);
            mSignCommentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new LoginDialogFragment()
                            .show(getActivity().getSupportFragmentManager(), IntentUtils.EXTRA_LOGIN_DIALOG);
                }
            });

            mCommentEditText.setVisibility(View.GONE);
            mSendButton.setVisibility(View.GONE);
        } else {
            mSignCommentButton.setVisibility(View.GONE);
            mCommentEditText.setVisibility(View.VISIBLE);
            mSendButton.setVisibility(View.VISIBLE);
        }

        mSendButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               final String comment = mCommentEditText.getText().toString();
               final JsonObject jsonComment = new JsonObject();
               jsonComment.addProperty("body", comment);

               final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
               final String username = sharedPref.getString("Username", "");
               final String password = sharedPref.getString("Password", "");

               final GistApiService gistApiService =
                       ServiceGenerator.createService(GistApiService.class,
                               username, password);

               Call<Comment> call = gistApiService.insertComment(mGitId, jsonComment);
               call.enqueue(new Callback<Comment>() {
                   @Override
                   public void onResponse(Call<Comment> call, Response<Comment> response) {
                       if (response.isSuccessful()) {
                           final Comment comment = response.body();

                           addCommentView(comment);

                           mCommentEditText.getText().clear();

                           InputMethodManager inputManager =
                                   (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                           inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                                   InputMethodManager.HIDE_NOT_ALWAYS);

                           Toast.makeText(getActivity(), R.string.comment_inserted, Toast.LENGTH_SHORT).show();

                           Log.d("DEBUG", "Comment inserted");
                       } else {
                           AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                           try {
                               builder.setMessage(response.errorBody().string())
                                       .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                                           public void onClick(DialogInterface dialog, int id) {
                                               getActivity().finish();
                                               startActivity(getActivity().getIntent());
                                           }
                                       })
                                       .setTitle(R.string.error);
                           } catch (IOException e) {
                               e.printStackTrace();
                           }

                           AlertDialog dialog = builder.create();
                           dialog.show();
                       }
                   }

                   @Override
                   public void onFailure(Call<Comment> call, Throwable t) {
                       Log.e("DEBUG", t.toString());

                       AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                       builder.setMessage(t.getMessage())
                               .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                                   public void onClick(DialogInterface dialog, int id) {
                                       getActivity().finish();
                                       startActivity(getActivity().getIntent());
                                   }
                               })
                               .setTitle(R.string.error);

                       AlertDialog dialog = builder.create();
                       dialog.show();
                   }
               });
           }
       });

        loadDataFromApi();
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().setTitle(R.string.gist);
    }

    @Override public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(COMMENT_EDIT_TEXT, mCommentEditText.getText().toString());
    }
    //endregion

    //region private methods
    private void updateViews(Gist gist) {
        mUserNameTextView.setText(gist.getUser().getLogin());
        mDetailTextView.setText(gist.getDescription());
        Glide.with(getActivity()).load(gist.getUser().getAvatarUrl()).into(mAvatarImageView);

        Iterator<Map.Entry<String, JsonElement>> iterator = gist.getFiles().entrySet().iterator();
        while(iterator.hasNext()) {
            final JsonElement jsonElement = iterator.next().getValue();
            if (mFileView.getText() != null) {
                String content = mFileView.getText() +
                        jsonElement.getAsJsonObject().get("content").getAsString();
                mFileView.setText(content);
            } else {
                mFileView.setText(jsonElement.getAsJsonObject().get("content").getAsString());
            }
        }
    }

    private void loadDataFromApi() {
        final GistApiService gistApiService =
                ServiceGenerator.createService(GistApiService.class);

        //load git selected
        gistApiService.getGist(mGitId).enqueue(new Callback<Gist>() {
            @Override
            public void onResponse(Call<Gist> call, Response<Gist> response) {
                if (response.isSuccessful()) {
                    final Gist gist = response.body();
                    updateViews(gist);

                    Log.d("DEBUG", "gist: " + gist.getId());

                    //load all the comments
                    if (gist.getComments() > 0) {
                        gistApiService.getComments(mGitId).enqueue(new Callback<List<Comment>>() {
                            @Override
                            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                                if (response.isSuccessful()) {
                                    final List<Comment> comments = response.body();

                                    for (Comment comment : comments) {
                                        addCommentView(comment);
                                    }

                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    try {
                                        builder.setMessage(response.errorBody().string())
                                                .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        getActivity().finish();
                                                        startActivity(getActivity().getIntent());
                                                    }
                                                })
                                                .setTitle(R.string.error);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                }
                            }

                            @Override
                            public void onFailure(Call<List<Comment>> call, Throwable t) {
                                Log.e("DEBUG", t.toString());
                            }
                        });
                    }

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    try {
                        builder.setMessage(response.errorBody().string())
                                .setTitle(R.string.error);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }

            @Override
            public void onFailure(Call<Gist> call, Throwable t) {
                Log.e("DEBUG", t.toString());

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(t.getMessage())
                        .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getActivity().finish();
                                startActivity(getActivity().getIntent());
                            }
                        })
                        .setTitle(R.string.error);

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    private void addCommentView(Comment comment) {
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0,10,0,10);

        final TextView textView = new TextView(getContext());
        textView.setText(comment.getUser().getLogin() +
                "  :  " + comment.getBody());

        final CardView cardView = new CardView(getContext());
        cardView.addView(textView);
        cardView.setLayoutParams(params);

        mCommentsLinearLayout.addView(cardView);
    }
    //endregion
}