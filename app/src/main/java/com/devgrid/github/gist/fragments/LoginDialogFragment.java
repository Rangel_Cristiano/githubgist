package com.devgrid.github.gist.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.devgrid.github.gist.R;
import com.devgrid.github.gist.models.Login;
import com.devgrid.github.gist.models.User;
import com.devgrid.github.gist.services.GistApiService;
import com.devgrid.github.gist.services.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Response;

public class LoginDialogFragment extends DialogFragment
        implements HeadlessFragment.HeadlessAsyncTask.Callback<Login, Login>, View.OnClickListener {

    private static final String STATE_MODEL = "stateModel";

    private HeadlessFragment mHeadless;

    private View mPanel;
    private EditText mUsername;
    private EditText mPassword;
    private TextView mError;
    private View mProgress;
    private Context mContext;

    private LoginAsyncTask mTask;
    private Login mModel;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mContext = getContext();

        if(savedInstanceState != null){
            mModel = (Login)savedInstanceState.getSerializable(STATE_MODEL);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_MODEL, mModel);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        mHeadless = HeadlessFragment.get(getFragmentManager());
        mTask = (LoginAsyncTask)mHeadless.getBackgroundTask();
        if(mTask != null){
            mTask.setCallback(this);
        }
        updateViewState();
    }

    @Override
    public void onCancel(DialogInterface dialog)
    {
        super.onCancel(dialog);
        if(mTask != null){
            mTask.cancel(false);
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        if(mTask != null){
            mTask.setCallback(null);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setTitle("Login");
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.dialog_login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        mPanel    = view.findViewById(R.id.panel);
        mUsername = (EditText)view.findViewById(R.id.username);
        mPassword = (EditText)view.findViewById(R.id.password);
        mError    = (TextView)view.findViewById(R.id.error);
        mProgress = view.findViewById(R.id.progress);
        setButton(DialogInterface.BUTTON_NEUTRAL, "Login", this);
    }

    private boolean setButton(int which, CharSequence text, View.OnClickListener listener)
    {
        final int id;
        switch(which){
            case DialogInterface.BUTTON_POSITIVE: id = android.R.id.button1; break;
            case DialogInterface.BUTTON_NEUTRAL:  id = android.R.id.button2; break;
            case DialogInterface.BUTTON_NEGATIVE: id = android.R.id.button3; break;
            default: throw new IllegalArgumentException("Bad button code");
        }
        final Button button = (Button)getView().findViewById(id);
        if(button != null){
            button.setText(text);
            button.setOnClickListener(listener);
            button.setVisibility(listener != null ? View.VISIBLE : View.GONE);
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v)
    {
        mTask = new LoginAsyncTask();
        mHeadless.setBackgroundTask(mTask);
        mTask.setCallback(this);
        mModel = new Login(
                mUsername.getText().toString(),
                mPassword.getText().toString());
        mTask.execute(mModel);
    }

    private void updateViewState()
    {
        boolean waiting = (mModel != null) && mModel.waiting;
        mPanel.setVisibility(waiting ? View.INVISIBLE : View.VISIBLE);
        mProgress.setVisibility(waiting ? View.VISIBLE : View.INVISIBLE);
        mError.setText((mModel != null && mModel.error != null) ?
                mModel.error.getMessage() : null);
    }

    @Override
    public void onPreExecute()
    {
        updateViewState();
    }

    @Override
    public void onProgressUpdate(Login... values)
    {
        // Do nothing
    }

    @Override
    public void onPostExecute(Login result)
    {
        mModel = result;
        updateViewState();

        if (result.success) {
            //add user on sharedPreferences to no more login
            final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean("Registered", true);
            editor.putString("Username", result.username);
            editor.putString("Password", result.password);
            editor.putString("Avatar", result.getAvatar());
            editor.putString("Login", result.getLogin());

            editor.apply();

            dismiss();

            //reload activity to load user informations
            getActivity().finish();
            startActivity(getActivity().getIntent());
        }
    }

    @Override
    public void onCancelled(Login result)
    {
        // do nothing
    }


    private static class LoginAsyncTask extends HeadlessFragment.HeadlessAsyncTask<Login, Login, Login>
    {
        @Override
        protected Login doInBackground(Login... params)
        {
            final Login model = params[0];
            final GistApiService gistApiService =
                    ServiceGenerator.createService(GistApiService.class,
                            model.username, model.password);
            Call<User> call = gistApiService.getMyUser();
            call.enqueue(new retrofit2.Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        final User user = response.body();

                        model.setAvatar(user.getAvatarUrl());
                        model.setEmail(user.getEmail());
                        model.setLogin(user.getLogin());

                        model.success = true;

                        onPostExecute(model);
                    } else {
                        model.waiting = false;
                        model.error =  new IllegalArgumentException("Bad username or password");

                        onPostExecute(model);
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    model.waiting = false;
                    model.error = t;

                    onPostExecute(model);
                }
            });

            return model;
        }
    }
}
