package com.devgrid.github.gist.services;

import com.devgrid.github.gist.models.Comment;
import com.devgrid.github.gist.models.Gist;
import com.devgrid.github.gist.models.GistPreview;
import com.devgrid.github.gist.models.User;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GistApiService {

    @GET("gists/public")
    Call<List<GistPreview>> getPublicGists(@Query("page") int pageIndex);

    @GET("gists/{id}")
    Call<Gist> getGist(@Path("id") String gitIndex);

    @GET("gists/{id}/comments")
    Call<List<Comment>> getComments(@Path("id") String gitIndex);

    @POST("gists/{id}/comments")
    Call<Comment> insertComment(@Path("id") String gitIndex, @Body JsonObject jsonObject);

    @GET("user")
    Call<User> getMyUser();
}
