package com.devgrid.github.gist.utils;

public final class IntentUtils {

    //region Fields

    public static final String EXTRA_KEY_QR_CODE = "KEY_QR_CODE";
    public static final String EXTRA_GIT_ID = "KEY_QR_CODE";
    public static final String EXTRA_LOGIN_DIALOG = "LOGIN_DIALOG";

    //endregion

    //region Constructors

    private IntentUtils() { throw new AssertionError("No instances for you!"); }

    //endregion
}
