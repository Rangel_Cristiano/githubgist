package com.devgrid.github.gist.models;

public class Authorization {

    private static Authorization mInstance = null;
    private String mAuthorization;

    protected Authorization() { }

    public static Authorization getInstance() {
        if(mInstance == null) {
            mInstance = new Authorization();
        }
        return mInstance;
    }

    public void setAuthorization(String authorization) {
        mAuthorization = authorization;
    }

    public String getAuthorization() {
        return mAuthorization;
    }
}
