package com.devgrid.github.gist.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.devgrid.github.gist.R;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.NonReusable;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@NonReusable
@Layout(R.layout.drawer_header)
public class DrawerHeaderAdapterView {

    @View(R.id.profile_image_view)
    private ImageView mProfileImage;

    @View(R.id.name_text_view)
    private TextView mNameTxt;

    @View(R.id.email_text_view)
    private TextView mEmailTxt;

    private Context mContext;

    public DrawerHeaderAdapterView(Context context) {
        mContext = context;
    }

    @Resolve
    private void onResolved() {
        updateViews();
    }

    public void updateViews() {
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        final Boolean registered = sharedPref.getBoolean("Registered", false);
        if (registered == true) {
            final String login = sharedPref.getString("Login", "");
            final String userName = sharedPref.getString("Username", "");
            final String avatar = sharedPref.getString("Avatar", "");

            mNameTxt.setText(login);
            mEmailTxt.setText(userName);
            Glide.with(mContext).load(avatar).into(mProfileImage);
        } else {
            mNameTxt.setText("Anonymous mode");
            mEmailTxt.setText("");
        }
    }
}