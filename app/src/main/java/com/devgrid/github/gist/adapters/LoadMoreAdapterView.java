package com.devgrid.github.gist.adapters;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.devgrid.github.gist.R;
import com.devgrid.github.gist.fragments.PublicGistsListFragment;
import com.devgrid.github.gist.models.GistPreview;
import com.mindorks.placeholderview.InfinitePlaceHolderView;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.infinite.LoadMore;

import java.util.List;

@Layout(R.layout.load_more_view)
public class LoadMoreAdapterView {

    public static final int LOAD_VIEW_SET_COUNT = 10;

    private InfinitePlaceHolderView mLoadMoreView;
    private List<GistPreview> mFeedList;
    private LoadMoreCallback mCallBack;
    private PublicGistsListFragment.OnGistListener mGistSelectListener;

    public LoadMoreAdapterView(InfinitePlaceHolderView loadMoreView,
                               List<GistPreview> feedList, PublicGistsListFragment.OnGistListener onGistListener) {

        mLoadMoreView = loadMoreView;
        mFeedList = feedList;
        mGistSelectListener = onGistListener;
    }

    public void addNewFeedListLoaded(List<GistPreview> feedList) {
        if (mFeedList == null) {
            mFeedList = feedList;
        } else {
            mFeedList.addAll(feedList);
        }
    }

    @LoadMore
    private void onLoadMore() {
        Log.d("DEBUG", "onLoadMore");
        new ForcedWaitedLoading();
    }

    public void setLoadMoreCallback(LoadMoreCallback callBack) {
        mCallBack = callBack;
    }

    class ForcedWaitedLoading implements Runnable {

        public ForcedWaitedLoading() {
            new Thread(this).start();
        }

        @Override
        public void run() {

            try {
                Thread.currentThread().sleep(1500);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    int count = mLoadMoreView.getViewCount();
                    Log.d("DEBUG", "count " + count);
                    Log.d("DEBUG", "list size " + mFeedList.size());

                    for (int i = count - 1; i < (count - 1 + LoadMoreAdapterView.LOAD_VIEW_SET_COUNT)
                            && mFeedList.size() > i; i++) {

                        final GistItemAdapterView gistItemAdapterView = new GistItemAdapterView(mLoadMoreView.getContext(), mFeedList.get(i));
                        mLoadMoreView.addView(gistItemAdapterView);

                        gistItemAdapterView.setLoadMoreCallback(new GistItemAdapterView.ItemViewCallback() {
                            @Override
                            public void onGistSelected(GistPreview gistPreview) {
                               mGistSelectListener.onSelectGist(gistPreview);
                            }
                        });

                        if(i == mFeedList.size() - 1) {

                            if (mCallBack != null) {
                                mCallBack.onLoadMore();
                            } else {
                                mLoadMoreView.noMoreToLoad();
                            }

                            break;
                        }
                    }
                    mLoadMoreView.loadingDone();
                }
            });
        }
    }

    public interface LoadMoreCallback {
        void onLoadMore();
    }
}
