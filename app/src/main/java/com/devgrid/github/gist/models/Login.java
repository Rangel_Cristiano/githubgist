package com.devgrid.github.gist.models;

import java.io.Serializable;

public class Login implements Serializable
{
    private static final long serialVersionUID = 1L;

    public String username;
    public String password;
    public String avatar;
    public String email;
    public Throwable error;
    public boolean success;
    public boolean waiting;
    private String login;

    public Login(String username, String password)
    {
        this.username = username;
        this.password = password;
        this.waiting  = true;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}