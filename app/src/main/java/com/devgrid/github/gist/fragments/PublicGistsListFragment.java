package com.devgrid.github.gist.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devgrid.github.gist.R;
import com.devgrid.github.gist.models.GistPreview;
import com.devgrid.github.gist.services.GistApiService;
import com.devgrid.github.gist.services.ServiceGenerator;
import com.devgrid.github.gist.adapters.GistItemAdapterView;
import com.devgrid.github.gist.adapters.LoadMoreAdapterView;
import com.mindorks.placeholderview.InfinitePlaceHolderView;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PublicGistsListFragment extends Fragment {

    //region Fields
    private OnGistListener mGistSelectListener;
    private InfinitePlaceHolderView mInfinitePlaceHolderView;
    private LoadMoreAdapterView mLoadMoreAdapterView;
    private int mPageIndex = 1;
    //endregion

    //region Lifecycle
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.public_gists_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mInfinitePlaceHolderView = (InfinitePlaceHolderView)getView().findViewById(R.id.loadMoreView);

        loadDataFromApi();
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().setTitle(R.string.public_gists);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    //endregion

    //region Getters & Setters
    public void setGistsListListener(OnGistListener addressSelectListener) {
        mGistSelectListener = addressSelectListener;
    }
    //endregion

    //region private methods
    private void loadDataFromApi() {
        //load public gists from api using pagination
        //only load 30 gists for round
        final GistApiService gistApiService = ServiceGenerator.createService(GistApiService.class);
        gistApiService.getPublicGists(mPageIndex).enqueue(new Callback<List<GistPreview>>() {
            @Override
            public void onResponse(Call<List<GistPreview>> call, Response<List<GistPreview>> response) {
                if (response.isSuccessful()) {
                    final List<GistPreview> gists = response.body();
                    setupView(gists);

                    Log.d("DEBUG", "Number of gists received: " + gists.size());
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    try {
                        builder.setMessage(response.errorBody().string())
                                .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        getActivity().finish();
                                        startActivity(getActivity().getIntent());
                                    }
                                })
                                .setTitle(R.string.error);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }

            @Override
            public void onFailure(Call<List<GistPreview>> call, Throwable t) {
                Log.e("DEBUG", t.toString());

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(t.getMessage())
                        .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getActivity().finish();
                                startActivity(getActivity().getIntent());
                            }
                        })
                        .setTitle(R.string.error);

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    private void setupView(List<GistPreview> gistList) {
        if (this.getActivity() != null) {
            for (int i = 0; i < LoadMoreAdapterView.LOAD_VIEW_SET_COUNT; i++) {
                GistItemAdapterView gistItemAdapterView = new GistItemAdapterView(this.getActivity().getApplicationContext(), gistList.get(i));

                mInfinitePlaceHolderView.addView(gistItemAdapterView);

                gistItemAdapterView.setLoadMoreCallback(new GistItemAdapterView.ItemViewCallback() {
                    @Override
                    public void onGistSelected(GistPreview gistPreview) {
                        mGistSelectListener.onSelectGist(gistPreview);
                    }
                });
            }

            //load 10 gists view for scroll
            if (mLoadMoreAdapterView == null) {
                mLoadMoreAdapterView = new LoadMoreAdapterView(mInfinitePlaceHolderView, gistList, mGistSelectListener);

                mLoadMoreAdapterView.setLoadMoreCallback(new LoadMoreAdapterView.LoadMoreCallback() {
                    @Override
                    public void onLoadMore() {
                        mPageIndex++;

                        loadDataFromApi();
                    }
                });
            } else {
                mLoadMoreAdapterView.addNewFeedListLoaded(gistList);
            }

            mInfinitePlaceHolderView.setLoadMoreResolver(mLoadMoreAdapterView);
        }
    }
    //endregion

    //region Listener
    public interface OnGistListener {
        void onSelectGist(GistPreview gistPreview);
    }
    //endregion
}
