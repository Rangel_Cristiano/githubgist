package com.devgrid.github.gist.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.devgrid.github.gist.R;
import com.devgrid.github.gist.models.Authorization;
import com.devgrid.github.gist.models.User;
import com.devgrid.github.gist.services.GistApiService;
import com.devgrid.github.gist.services.ServiceGenerator;
import com.devgrid.github.gist.utils.IntentUtils;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QrCodeScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        // Programmatically initialize the scanner view
        mScannerView = new ZXingScannerView(this);
        // Set the scanner view as the content view
        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Register ourselves as a handler for scan results.
        mScannerView.setResultHandler(this);
        // Start camera on resume
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        // Stop camera on pause
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        Log.d("result", rawResult.getText());

        //set the otpath return
        Authorization.getInstance().setAuthorization(rawResult.getText());

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(rawResult.getText())
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setTitle(rawResult.getBarcodeFormat().toString());

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
