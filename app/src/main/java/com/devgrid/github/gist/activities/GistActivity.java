package com.devgrid.github.gist.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.devgrid.github.gist.R;
import com.devgrid.github.gist.fragments.GistFragment;
import com.devgrid.github.gist.utils.IntentUtils;

public class GistActivity extends AppCompatActivity {

    private String mGistId;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gist_activity);

        mGistId = getIntent().getStringExtra(IntentUtils.EXTRA_GIT_ID);

        final GistFragment gistFragment = new GistFragment();
        final Bundle bundle = new Bundle();
        bundle.putString(IntentUtils.EXTRA_GIT_ID, mGistId);
        gistFragment.setArguments(bundle);

        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_content, gistFragment);
        fragmentTransaction.commit();
    }
}
