package com.devgrid.github.gist.adapters;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.devgrid.github.gist.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@Layout(R.layout.drawer_item)
public class DrawerMenuItemAdapterView {

    public static final int DRAWER_MENU_ITEM_PUBLIC_GISTS = 1;
    public static final int DRAWER_MENU_ITEM_QRCODE= 2;
    public static final int DRAWER_MENU_ITEM_LOGIN = 3;
    public static final int DRAWER_MENU_ITEM_LOGOUT = 4;

    private int mMenuPosition;
    private Context mContext;
    private DrawerCallBack mCallBack;

    @View(R.id.item_text_view)
    private TextView mItemNameTxt;

    @View(R.id.icon_image_view)
    private ImageView mItemIcon;

    public DrawerMenuItemAdapterView(Context context, int menuPosition, DrawerCallBack callBack) {
        mContext = context;
        mMenuPosition = menuPosition;
        mCallBack = callBack;
    }

    //invert login / logout
    public void changeLoginLogout(int newMenuPosition) {
        switch (mMenuPosition) {
            case DRAWER_MENU_ITEM_LOGIN:
                mItemIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_login));
                mItemNameTxt.setText(R.string.sign_out);
                break;
            case DRAWER_MENU_ITEM_LOGOUT:
                mItemIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_login));
                mItemNameTxt.setText(R.string.sign_out);
                break;
        }
        mMenuPosition = newMenuPosition;
    }

    public void updateViews() {
        switch (mMenuPosition){
            case DRAWER_MENU_ITEM_PUBLIC_GISTS:
                mItemNameTxt.setText(R.string.public_gists);
                mItemIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_list));
                break;
            case DRAWER_MENU_ITEM_QRCODE:
                mItemIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_qr_code));
                mItemNameTxt.setText(R.string.qrcode);
                break;
            case DRAWER_MENU_ITEM_LOGIN:
                mItemIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_login));
                mItemNameTxt.setText(R.string.sign_in);
                break;
            case DRAWER_MENU_ITEM_LOGOUT:
                mItemIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_login));
                mItemNameTxt.setText(R.string.sign_out);
                break;
        }
    }

    @Resolve
    private void onResolved() {
        updateViews();
    }

    @Click(R.id.menu_view)
    private void onMenuItemClick(){
        switch (mMenuPosition){
            case DRAWER_MENU_ITEM_PUBLIC_GISTS:
                if (mCallBack != null) mCallBack.onPublicGistsMenuSelected();
                break;
            case DRAWER_MENU_ITEM_QRCODE:
                if (mCallBack != null) mCallBack.onQrCodeAuthentication();
                break;
            case DRAWER_MENU_ITEM_LOGIN:
                if (mCallBack != null) mCallBack.onLoginMenuSelected();
                break;
            case DRAWER_MENU_ITEM_LOGOUT:
                if (mCallBack != null) mCallBack.onLogoutMenuSelected();
                break;
        }
    }

    public void setDrawerCallBack(DrawerCallBack callBack) {
        mCallBack = callBack;
    }

    public interface DrawerCallBack {
        void onPublicGistsMenuSelected();
        void onQrCodeAuthentication();
        void onLoginMenuSelected();
        void onLogoutMenuSelected();

    }
}