package com.devgrid.github.gist.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Gist extends GistPreview implements Serializable {

    @SerializedName("comments")
    @Expose
    private int mComments;

    @SerializedName("files")
    @Expose
    private JsonObject mFiles;

    public int getComments() {
        return mComments;
    }

    public void setComments(int comments) {
        mComments = comments;
    }

    public void setFiles(JsonObject files) {
        mFiles = files;
    }

    public JsonObject getFiles() {
        return mFiles;
    }
}