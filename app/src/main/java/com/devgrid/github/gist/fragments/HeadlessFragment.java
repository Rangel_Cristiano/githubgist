package com.devgrid.github.gist.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public class HeadlessFragment extends Fragment {

    private HeadlessAsyncTask<?, ?, ?> mBackgroundTask;

    public static HeadlessFragment get(FragmentManager fm) {
        HeadlessFragment headless = (HeadlessFragment) fm.findFragmentByTag("headless");
        if (headless == null) {
            headless = new HeadlessFragment();
            fm.beginTransaction()
                    .add(headless, "headless")
                    .commit();
        }
        return headless;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setBackgroundTask(HeadlessAsyncTask<?, ?, ?> backgroundTask) {
        mBackgroundTask = backgroundTask;
    }

    public HeadlessAsyncTask<?, ?, ?> getBackgroundTask() {
        return mBackgroundTask;
    }

    abstract static class HeadlessAsyncTask<Param, Progress, Result> extends AsyncTask<Param, Progress, Result>
    {
        private Callback<Progress, Result> mCallback;


        public void setCallback(Callback<Progress, Result> callback)
        {
            mCallback = callback;
        }

        @Override
        protected void onPreExecute()
        {
            if(mCallback != null){
                mCallback.onPreExecute();
            }
        }

        @Override
        protected void onProgressUpdate(Progress... values)
        {
            if(mCallback != null){
                mCallback.onProgressUpdate(values);
            }
        }

        @Override
        protected void onPostExecute(Result result)
        {
            if(mCallback != null){
                mCallback.onPostExecute(result);
            }
        }

        @Override
        protected void onCancelled(Result result)
        {
            if(mCallback != null){
                mCallback.onCancelled(result);
            }
        }

        public interface Callback<Progress, Result>
        {
            public void onPreExecute    ();
            public void onProgressUpdate(Progress... values);
            public void onPostExecute   (Result result);
            public void onCancelled     (Result result);
        }
    }
}
